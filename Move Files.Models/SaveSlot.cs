﻿using System;

namespace Move_Files.Models
{
    public class SaveSlot
    {
        public string FileType { get; set; }
        public string RootFolder { get; set; }
        public string DestinationFolder { get; set; }
    }
}
