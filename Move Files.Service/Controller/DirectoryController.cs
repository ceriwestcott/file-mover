﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Move_Files.Controller;
using Move_Files.Models;

namespace Move_Files
{
    public class DirectoryController : IController
    {
 
        private string initialFolder = String.Empty;
        private string fileType = string.Empty;
        private string destinationFolder = string.Empty;
        private string currentDirectory = string.Empty;
        private List<SaveSlot> existingConfig;
        private List<string> loadedDirectories;

        public string RootDirectory
        {
            get { return initialFolder; }
            set { initialFolder = value; }
        }
        
    
        public string FileType
        {
            get { return fileType;}
            set { fileType = value; }
        }

        public string DestinationFolder
        {
            get { return destinationFolder; }
            set { destinationFolder = value; }
        }

        public DirectoryController(string fileType, string initialFolder, string destinationFolder )
        {
            RootDirectory = initialFolder;
            FileType = fileType;
            DestinationFolder = destinationFolder;

            loadedDirectories = new List<string>();
        }

        public DirectoryController(List<SaveSlot> existingConfig)
        {
            this.existingConfig = existingConfig;
        }

        [Obsolete("Currently depreciated - All functionality combined within Move Files Method, Will come back to this one day")]
        public  void ListFilesInDirectory()
        {
            var userInput = string.Empty;
            for (int i = 0; i < loadedDirectories.Count; i++)
            {
                Console.WriteLine(i + 1 + " " + loadedDirectories[i]);
            }

            userInput = Console.ReadLine();

            string[] fileInDirectory = System.IO.Directory.GetFiles(loadedDirectories[Convert.ToInt32(userInput)-1]);

            foreach (var file in fileInDirectory)
            {
                Console.WriteLine(file);
            }
        }

        public void MoveFiles(TextBox tbConsole)
        {
            tbConsole.Text += "\nPreparing to Move\n";
            // Enumerate required files...
            foreach (var file in Directory.EnumerateFiles(RootDirectory, "*." + FileType))
            {
                var newFileDestination = Path.Combine(destinationFolder, Path.GetFileName(file));

                if (File.Exists(newFileDestination))
                    File.Delete(newFileDestination);
                // ...and move/copy them
                // File.Copy if you want to copy
                File.Move(file, newFileDestination);
                BuildLogMessage(tbConsole, file, newFileDestination);
            }
        }

        public void MoveFiles(string fileType, string rootFolder, string destinationFolder)
        {
            foreach (var file in Directory.EnumerateFiles(rootFolder, "*." + fileType))
            {
                var newFileDestination = Path.Combine(destinationFolder, Path.GetFileName(file));

                if (File.Exists(newFileDestination))
                    File.Delete(newFileDestination);
                // ...and move/copy them
                // File.Copy if you want to copy
                File.Move(file, newFileDestination);
            }
        }


        private void BuildLogMessage(TextBox tbConsole, string file, string newFileDestination)
        {
           StringBuilder sb = new StringBuilder();
            sb.AppendLine(" ");
            sb.AppendLine("File: " + file);
            sb.AppendLine("has moved to");
            sb.AppendLine("File: " + newFileDestination);
            sb.AppendLine("---------------------------------------------------------------------------------------------------------------------------------------");

            tbConsole.Text += sb.ToString();
        }


        [Obsolete("Currently depreciated - All functionality combined within Move Files Method")]
        public  void LoadDirectories()
        {
            try
            {
                loadedDirectories = System.IO.Directory.GetDirectories(initialFolder).ToList();
            }
            catch (DirectoryNotFoundException exception)
            {
                Console.WriteLine(exception.StackTrace);
            }
        }

        public  void PrintDirectory()
        {
            foreach (var loadedDirectory in loadedDirectories)
            {
                Console.WriteLine(loadedDirectory);
            }
        }

        public void MoveFilesUsingConfig()
        {
            foreach (var configItem in existingConfig)
            {
                MoveFiles(configItem.FileType, configItem.RootFolder, configItem.DestinationFolder);
            }
        }
    }
}
