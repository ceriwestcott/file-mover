﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Move_Files.Models;
using Newtonsoft.Json;

namespace Move_Files.Service.Controller
{
    public class FileController
    {
        private List<SaveSlot> exisitingConfig;
        private string saveDirectory;
        public void SaveNewConfig(string fileType, string rootFolder, string destinationFolder)
        {
           
        }

        public void SaveNewConfig(SaveSlot configToSave)
        {
            exisitingConfig = new List<SaveSlot>();
            ValidateSaveLocation();
            DeserializeConfig();
            exisitingConfig.Add(configToSave);

            var serializedJson = JsonConvert.SerializeObject(exisitingConfig);

            File.WriteAllText(saveDirectory + "\\SaveConfig.json", serializedJson);

        }

        private void ValidateSaveLocation()
        {
            saveDirectory = Path.GetDirectoryName(Application.ExecutablePath);
            if (!File.Exists(saveDirectory + "\\SaveConfig.json"))
                File.Create(saveDirectory + "\\SaveConfig.json").Close();
        }

        private void DeserializeConfig()
        {
            ValidateSaveLocation();
            exisitingConfig =
                JsonConvert.DeserializeObject<List<SaveSlot>>(File.ReadAllText(saveDirectory + "\\SaveConfig.json"));
            if (exisitingConfig == null)
            {
                exisitingConfig = new List<SaveSlot>();
            }

        }

        public void RunConfigRules()
        {
            DeserializeConfig();
            if (exisitingConfig.Count > 0)
            {
                DirectoryController directoryController = new DirectoryController(exisitingConfig);
                directoryController.MoveFilesUsingConfig();
            }
        }
    }
}
