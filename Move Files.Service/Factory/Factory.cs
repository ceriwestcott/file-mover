﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Move_Files.Controller;
using Move_Files.Enum;

namespace Move_Files.Factory
{
    public class Factory
    {
        //If you read this, this is the completely the wrong way of implementing a factory pattern
        //Infact, I 100% don't even need to do this
        public IController GetObject(ObjectType objectType, string fileType, string initialFolder, string destinationFolder)
        {
            switch (objectType)
            {
                case ObjectType.DIRECTORY_CONTROLLER:
                    return new DirectoryController(fileType, initialFolder, destinationFolder);
                    break;
            }

            return new DirectoryController(fileType, initialFolder, destinationFolder);
        }
    }
}
