﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Move_Files;
using Move_Files.Enum;
using Move_Files.Factory;
using Move_Files.Models;
using Move_Files.Service.Controller;

namespace Move_Files_GUI
{
    public partial class Form1 : Form
    {
        private readonly Factory factory;
        private readonly DirectoryController directoryController;

        public Form1()
        {
            InitializeComponent();
            factory = new Factory();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tbRootFolder_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var rootFolder = tbRootFolder.Text;
            var destinationFolder = tbDestinationFolder.Text;
            if (!Directory.Exists(rootFolder))
                tbConsole.Text+="\nFATAL: Root Directory does not exist";
            else if (!Directory.Exists(destinationFolder))
                tbConsole.Text += "\n FATAL: Destination Directory does not exist";
            else tbConsole.Text += "\n Directories Exist";
        }

        private void tbMoveFiles_Click(object sender, EventArgs e)
        {
            DirectoryController directoryController = new DirectoryController(tbFileType.Text,
                tbRootFolder.Text,
                tbDestinationFolder.Text);
            directoryController.MoveFiles(tbConsole);
        }

        private void tbDestinationFolder_TextChanged(object sender, EventArgs e)
        {

        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnRootFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdb = new FolderBrowserDialog();
            if (fdb.ShowDialog() == DialogResult.OK)
            {
                tbRootFolder.Text = fdb.SelectedPath;
            }
        }

        private void btnDestinationFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdb = new FolderBrowserDialog();
            if (fdb.ShowDialog() == DialogResult.OK)
            {
                tbDestinationFolder.Text = fdb.SelectedPath;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FileController fileController = new FileController();
            fileController.RunConfigRules();
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            FileController fileController = new FileController();
            SaveSlot configToSave = new SaveSlot
            {
                FileType = tbFileType.Text,
                DestinationFolder = tbDestinationFolder.Text,
                RootFolder = tbRootFolder.Text
            };
            fileController.SaveNewConfig(configToSave);
        }
    }
}
