﻿namespace Move_Files_GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbFileType = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbRootFolder = new System.Windows.Forms.Label();
            this.tbRootFolder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbDestinationFolder = new System.Windows.Forms.TextBox();
            this.tbValidateFolderLocation = new System.Windows.Forms.Button();
            this.tbConsole = new System.Windows.Forms.TextBox();
            this.tbMoveFiles = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnRootFolder = new System.Windows.Forms.Button();
            this.btnDestinationFolder = new System.Windows.Forms.Button();
            this.btnRunConfigRules = new System.Windows.Forms.Button();
            this.btnSaveConfig = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbFileType
            // 
            this.tbFileType.Location = new System.Drawing.Point(107, 15);
            this.tbFileType.Name = "tbFileType";
            this.tbFileType.Size = new System.Drawing.Size(211, 20);
            this.tbFileType.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "File Type";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // lbRootFolder
            // 
            this.lbRootFolder.AutoSize = true;
            this.lbRootFolder.Location = new System.Drawing.Point(11, 41);
            this.lbRootFolder.Name = "lbRootFolder";
            this.lbRootFolder.Size = new System.Drawing.Size(62, 13);
            this.lbRootFolder.TabIndex = 3;
            this.lbRootFolder.Text = "Root Folder";
            // 
            // tbRootFolder
            // 
            this.tbRootFolder.Location = new System.Drawing.Point(107, 38);
            this.tbRootFolder.Name = "tbRootFolder";
            this.tbRootFolder.Size = new System.Drawing.Size(605, 20);
            this.tbRootFolder.TabIndex = 2;
            this.tbRootFolder.TextChanged += new System.EventHandler(this.tbRootFolder_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Destination Folder";
            // 
            // tbDestinationFolder
            // 
            this.tbDestinationFolder.Location = new System.Drawing.Point(107, 65);
            this.tbDestinationFolder.Name = "tbDestinationFolder";
            this.tbDestinationFolder.Size = new System.Drawing.Size(605, 20);
            this.tbDestinationFolder.TabIndex = 4;
            this.tbDestinationFolder.TextChanged += new System.EventHandler(this.tbDestinationFolder_TextChanged);
            // 
            // tbValidateFolderLocation
            // 
            this.tbValidateFolderLocation.Location = new System.Drawing.Point(569, 91);
            this.tbValidateFolderLocation.Name = "tbValidateFolderLocation";
            this.tbValidateFolderLocation.Size = new System.Drawing.Size(143, 23);
            this.tbValidateFolderLocation.TabIndex = 6;
            this.tbValidateFolderLocation.Text = "Validate Folder Location";
            this.tbValidateFolderLocation.UseVisualStyleBackColor = true;
            this.tbValidateFolderLocation.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbConsole
            // 
            this.tbConsole.Location = new System.Drawing.Point(12, 119);
            this.tbConsole.Multiline = true;
            this.tbConsole.Name = "tbConsole";
            this.tbConsole.Size = new System.Drawing.Size(818, 319);
            this.tbConsole.TabIndex = 7;
            // 
            // tbMoveFiles
            // 
            this.tbMoveFiles.Location = new System.Drawing.Point(718, 90);
            this.tbMoveFiles.Name = "tbMoveFiles";
            this.tbMoveFiles.Size = new System.Drawing.Size(112, 23);
            this.tbMoveFiles.TabIndex = 8;
            this.tbMoveFiles.Text = "Move Files";
            this.tbMoveFiles.UseVisualStyleBackColor = true;
            this.tbMoveFiles.Click += new System.EventHandler(this.tbMoveFiles_Click);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.HelpRequest += new System.EventHandler(this.folderBrowserDialog1_HelpRequest);
            // 
            // btnRootFolder
            // 
            this.btnRootFolder.Location = new System.Drawing.Point(718, 41);
            this.btnRootFolder.Name = "btnRootFolder";
            this.btnRootFolder.Size = new System.Drawing.Size(112, 20);
            this.btnRootFolder.TabIndex = 9;
            this.btnRootFolder.Text = "Select Folder";
            this.btnRootFolder.UseVisualStyleBackColor = true;
            this.btnRootFolder.Click += new System.EventHandler(this.btnRootFolder_Click);
            // 
            // btnDestinationFolder
            // 
            this.btnDestinationFolder.Location = new System.Drawing.Point(718, 65);
            this.btnDestinationFolder.Name = "btnDestinationFolder";
            this.btnDestinationFolder.Size = new System.Drawing.Size(112, 20);
            this.btnDestinationFolder.TabIndex = 10;
            this.btnDestinationFolder.Text = "Select Folder";
            this.btnDestinationFolder.UseVisualStyleBackColor = true;
            this.btnDestinationFolder.Click += new System.EventHandler(this.btnDestinationFolder_Click);
            // 
            // btnRunConfigRules
            // 
            this.btnRunConfigRules.Location = new System.Drawing.Point(12, 91);
            this.btnRunConfigRules.Name = "btnRunConfigRules";
            this.btnRunConfigRules.Size = new System.Drawing.Size(112, 23);
            this.btnRunConfigRules.TabIndex = 11;
            this.btnRunConfigRules.Text = "Run Config Rules";
            this.btnRunConfigRules.UseVisualStyleBackColor = true;
            this.btnRunConfigRules.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btnSaveConfig
            // 
            this.btnSaveConfig.Location = new System.Drawing.Point(130, 91);
            this.btnSaveConfig.Name = "btnSaveConfig";
            this.btnSaveConfig.Size = new System.Drawing.Size(112, 23);
            this.btnSaveConfig.TabIndex = 12;
            this.btnSaveConfig.Text = "Save Config Rule";
            this.btnSaveConfig.UseVisualStyleBackColor = true;
            this.btnSaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 474);
            this.Controls.Add(this.btnSaveConfig);
            this.Controls.Add(this.btnRunConfigRules);
            this.Controls.Add(this.btnDestinationFolder);
            this.Controls.Add(this.btnRootFolder);
            this.Controls.Add(this.tbMoveFiles);
            this.Controls.Add(this.tbConsole);
            this.Controls.Add(this.tbValidateFolderLocation);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbDestinationFolder);
            this.Controls.Add(this.lbRootFolder);
            this.Controls.Add(this.tbRootFolder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbFileType);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbFileType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbRootFolder;
        private System.Windows.Forms.TextBox tbRootFolder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbDestinationFolder;
        private System.Windows.Forms.Button tbValidateFolderLocation;
        private System.Windows.Forms.TextBox tbConsole;
        private System.Windows.Forms.Button tbMoveFiles;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button btnRootFolder;
        private System.Windows.Forms.Button btnDestinationFolder;
        private System.Windows.Forms.Button btnRunConfigRules;
        private System.Windows.Forms.Button btnSaveConfig;
    }
}

